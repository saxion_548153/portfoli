let bodyDiv
let portfolio
let addSectionButton
let hideShowButton

let selectedItem

let skill_box
let project_box

window.onload = init;

async function init() {
    let portfolioTitleObject = document.getElementById("portfolio_title");
    bodyDiv = document.getElementById("body_div");
    portfolio = document.getElementById("portfolio");
    addSectionButton = document.getElementById("addSectionButtonDiv");
    hideShowButton = document.getElementById("showHide");
    skill_box = document.getElementById("skill_box");
    project_box = document.getElementById("project_box");

    hideShowButton.onclick = function () {
        console.log(getHiddenState())

        let state = getHiddenState()
        if(state){
            changeSectorCss(false)
        }
        else{
            changeSectorCss(true)
        }
    }

    await populate_boxes()
    // Portfolio Main Title
    portfolioTitleObject.onclick = function (){
        createEditableLabel("portfolio_title", "portfolio",15);
    };

    addSectionButton.onclick = function (){

        let childCount = 0;
        let childrenPortfolio = portfolio.children
        for(let i = 0; i < childrenPortfolio.length; i++){
            if (childrenPortfolio[i].id.includes("emptySector")
                || childrenPortfolio[i].id.includes("newSector")){
                childCount++;
            }

        }
        console.log(childCount)
        insertNewSector(childCount +2,1,100,70,"Sector Title")
        insertEmptySector(childCount +3,1,100,70)
        addSectionButton.style.gridRow = `${portfolio.childElementCount + 1}`;
        let state = getHiddenState()
        if(state){
            changeSectorCss(true)
        }
        else{
            changeSectorCss(false)
        }
    }

    // Portfolio Starting design
    insertEmptySector(2,1,100,40)
    insertNewSector(3,1,100,70,"Skills")
    insertEmptySector(4,1,100,70)
    insertNewSector(5,1,100,70,"Projects")
    insertEmptySector(6,1,100,70)
}

function getHiddenState (){
    let elements = document.getElementsByClassName("emptySector");
    let elements2 = document.getElementsByClassName("removeButtonDiv");

    for (let i = 0; i < elements.length; i++) {
        let state = window.getComputedStyle(elements[i],null).visibility
        if (state === "hidden") {
            return true;
        }
    }

    for (let i = 0; i < elements2.length; i++) {
        let state = window.getComputedStyle(elements2[i],null).visibility
        if (state === "hidden") {
            return  true
        }
    }
    return false
}

function changeSectorCss(toHide){
    console.log(toHide)
    let elements = document.getElementsByClassName("emptySector");
    let elements2 = document.getElementsByClassName("removeButtonDiv");

    for (let i = 0; i < elements.length; i++) {

        if (toHide) {
            elements[i].style.visibility = "hidden"
            console.log("a")
        }
        else{
            elements[i].style.visibility = "visible"
            console.log("unhiding")
        }

    }

    for (let i = 0; i < elements2.length; i++) {
        if (toHide) {
            elements2[i].style.visibility = "hidden"
        }
        else{
            elements2[i].style.visibility = "visible"
        }

    }
}

function deleteElement(element, removeButtonDiv){
    element.remove()
    removeButtonDiv.remove()

    let children = portfolio.children
    let chCount = 0;
    for (let i = 0; i < children.length; i++) {
        if (children[i].id.includes("emptySector")){
            chCount++;
            let childElements = children[i].id.split(";")
            let buttonId = `${childElements[0]}` + ";" + `${childElements[1]}` + ";" + "_newRemoveButtonDiv"
            let button = document.getElementById(buttonId)
            button.style.gridRow = `${chCount+1}`
            button.style.gridColumn = `${2}`
            button.id = `${chCount+1}` + ';' +  `${2}` + ';' +  "_newRemoveButton"


            children[i].id = `${chCount+1}` + ';' +  `${1}` + ';' +  "_emptySector"
            children[i].style.gridRow = `${chCount+1}`
            children[i].style.gridColumn = `${1}`
        }
        else if (children[i].id.includes("newSector")){
            chCount++;
            let childElements = children[i].id.split(";")
            let buttonId = `${childElements[0]}` + ";" + `${childElements[1]}` + ";" + "_newRemoveButtonDiv"
            let button = document.getElementById(buttonId)

            button.style.gridRow = `${chCount+1}`
            button.style.gridColumn = `${2}`
            button.id = `${chCount+1}` + ';' +  `${2}` + ';' +  "_newRemoveButton"

            children[i].id = `${chCount+1}` + ';' +  `${1}` + ';' +  "_newSector"
            children[i].style.gridRow = `${chCount+1}`
            children[i].style.gridColumn = `${1}`
        }
    }
}

function insertEmptySector(row, column, height){
    let emptySector = document.createElement("div");

    let removeButton = document.createElement("button");
    let removeButtonDiv = document.createElement("div");

    let sizeLabel = document.createElement("p");
    let sizeLabelValue = document.createElement("p");

    sizeLabel.style.width
    sizeLabel.innerText = "Size "
    sizeLabel.className = "emptySectorSizeLabel";

    sizeLabelValue.className = "emptySectorSizeLabelValue";
    sizeLabelValue.innerText = height + "px"
    sizeLabelValue.id = `2` + ';' +  `2` + ';' +  "_emptySectorSizeLabelValue";

    emptySector.id = `${row}` + ';' +  `${column}` + ';' +  "_emptySector";
    emptySector.className = "emptySector";
    emptySector.style.height = height + "px";
    emptySector.style.gridRow = row;
    emptySector.style.gridColumn = column;


    // RemoveButton CSS
    removeButton.id = `${row}` + ';' +  `${column}` + ';' +  "_newRemoveButton"
    removeButton.className = "removeButton";
    removeButton.innerText = "-"

    // RemoveButtonDiv CSS
    removeButtonDiv.id = `${row}` + ';' +  `${column}` + ';' +  "_newRemoveButtonDiv"
    removeButtonDiv.className = "removeButtonDiv";
    removeButtonDiv.style.gridRow = row
    removeButton.onclick = function (){
        deleteElement(emptySector, removeButtonDiv)
    }

    sizeLabelValue.onclick = function (){
        createEditableLabel(sizeLabelValue.id,emptySector.id,0)
    }

    removeButtonDiv.appendChild(removeButton);
    document.getElementById("portfolio").appendChild(removeButtonDiv);
    emptySector.appendChild(sizeLabel)
    emptySector.appendChild(sizeLabelValue)
    portfolio.appendChild(emptySector);
    addSectionButton.style.gridRow = `${portfolio.childElementCount}`;
}

function insertNewSector(row, column, width, height, title){
    let newSector = document.createElement("div");
    let newSectorDiv = document.createElement("div")

    let newSectorTitle = document.createElement("p")
    let newSectorTitleDiv = document.createElement("div")

    let removeButton = document.createElement("button");
    let removeButtonDiv = document.createElement("div");

    let itemList = document.createElement("div");
    let addItemButton = document.createElement("button");



    itemList.style.backgroundColor = "white"
    itemList.style.width = "100%"
    itemList.style.height = "100%"
    itemList.style.display = "grid"
    itemList.style.gridTemplateRows = "1fr"
    itemList.style.gridTemplateColumns = "fit-content(24ch) fit-content(24ch) fit-content(24ch) fit-content(24ch)"
    itemList.id = `${row}` + ';' +  `${column}` + ';' +  "_itemList";

    // Sector CSS
    newSector.id = `${row}` + ';' +  `${column}` + ';' +  "_newSector";
    newSector.style.height = height + "px";
    newSector.className = "newSector";
    newSector.style.gridTemplateRows = "fit-content(8ch) 1fr"
    newSector.style.gap = "10px"

    addItemButton.className = "addItemButton";
    addItemButton.innerText = "+";

    // Title CSS
    newSectorTitle.id = `${row}` + ';' +`${column}` + ';' + "_newSectorTitle"
    newSectorTitle.innerText = title;
    newSectorTitle.className = "newSectorTitle";

    // Title Div CSS
    newSectorTitleDiv.id = `${row}` + ';' +  `${column}` + ';' +  "_newSectorTitleDiv"
    newSectorTitleDiv.className = "newSectorTitleDiv"

    // Sector Div CSS
    newSectorDiv.id = `${row}` + ';' +  `${column}` + ';' +  "_newSectorDiv"
    newSectorDiv.style.gridRow = row;
    newSectorDiv.style.gridColumn = column;
    newSectorDiv.className = "newSectorDiv"

    // RemoveButton CSS
    removeButton.id = `${row}` + ';' +  `${column}` + ';' +  "_newRemoveButton"
    removeButton.innerText = "X"
    removeButton.className = "removeButton";

    // RemoveButtonDiv CSS
    removeButtonDiv.id = `${row}` + ';' +  `${column}` + ';' +  "_newRemoveButtonDiv"
    removeButtonDiv.style.width = "50px"
    removeButtonDiv.style.gridRow = "2"
    removeButtonDiv.style.display = "grid"
    removeButtonDiv.style.justifyItems = "center"
    removeButtonDiv.className = "removeButtonDiv";
    removeButtonDiv.style.gridRow = row

    portfolio.appendChild(newSectorDiv);
    newSectorDiv.appendChild(newSectorTitleDiv);
    newSectorTitleDiv.appendChild(newSectorTitle);
    removeButtonDiv.appendChild(removeButton);
    document.getElementById("portfolio").appendChild(removeButtonDiv);
    newSector.appendChild(addItemButton);
    newSector.appendChild(itemList)
    newSectorDiv.appendChild(newSector);

    addItemButton.onclick = async function (){
        let itemDiv = document.createElement("div");
        let text = document.createElement("p");



        let itemToAdd = selectedItem.split(";")

        let headers = new Headers()
        headers.append("Content-Type", "application/json")


        if (itemToAdd[0] === "skill"){

            await add_skill_to_portfolio()
            //fetch skill
            await fetch('http://localhost:3000/skill/get?' + new URLSearchParams({
                    "id" : itemToAdd[1]
                }
            ).toString(),{
                method: 'GET',
                Headers: headers,

            }).then(function(response){
                if(response.ok){
                    response.json().then(function(json){
                        text.innerText = "Skill Name: " + json[0].name;
                    })
                }
            })
        }
        else if (itemToAdd[0] === "project"){
           await add_project_to_portfolio()
            // fetch project
            await fetch('http://localhost:3000/project/get?' + new URLSearchParams({
                    "id" :  itemToAdd[1]
                }
            ).toString(),{
                method: 'GET',
                Headers: headers,

            }).then(function(response){
                if(response.ok){
                    response.json().then(function(json){
                        text.innerText = "Project Name: " + json[0].name;
                    })
                }
            })
        }

        itemDiv.appendChild(text);
        itemList.appendChild(itemDiv);
    }

    removeButton.onclick = function (){
        deleteElement(newSectorDiv, removeButtonDiv)
    }
    newSectorTitle.onclick = function (){
        //dcreateEditableLabel(newSectorTitle.id,"portfolio",0);
        console.log(newSectorTitle.id)
    }
}

function createEditableLabel(Label, ObjectToAddTo, marginTop) {
    let row
    let column

     const valuesOfLabel = Label.split(";")
     if (ObjectToAddTo === "portfolio_title") {
         row = 1
         column = 1
     }
     else{
         row = valuesOfLabel[0];
         column = valuesOfLabel[1];
     }

    insertLabelTextBox(row, column, ObjectToAddTo)
    setEditableLabelProperties(row,column,Label,marginTop, ObjectToAddTo)
    setEditableLabelValue(row,column,Label, ObjectToAddTo)
}

// ObjectToAddTo is the ID of the div that the editableTextBox will be added to
function insertLabelTextBox(row, column, ObjectToAddTo){
    const div = document.createElement("div");
    div.id = `${row}` + ';' +  `${column}` + ';' +  "_editableLabelDiv";
    let inputId = `${row}` + ';' +  `${column}` + ';' +  "_editableLabel";
    div.innerHTML = `
    <input id="${inputId}" type="text"/>
    `;

    document.getElementById(`${ObjectToAddTo}`).appendChild(div)
}

// Label is the name of the ID of the <p> title itself
function setEditableLabelProperties(row, column, Label, marginTop, ObjectToAddTo) {
    let editBox= document.getElementById(`${row}` + ';' + `${column}` + ';' + "_editableLabel");
    let editBoxDiv = document.getElementById(`${row}` + ';' + `${column}` + ';' + "_editableLabelDiv");
    let LabelObject;
    if (ObjectToAddTo.includes("_emptySector")){
        let children = document.getElementById(ObjectToAddTo).children
        for (let i=0; i<children.length; i++){
            if (children[i].id === Label){
                LabelObject = children[i]
            }
        }
    }
    else{
        LabelObject = document.getElementById(Label);
    }

    // Set Editable box <div/> properties
    editBoxDiv.style.marginTop = `${marginTop}px`;
    if (Label !== "portfolio_title"){
        editBoxDiv.style.gridRow = row;
        editBoxDiv.style.gridColumn = column;
    }
    else{
        editBoxDiv.style.gridRow = "1";
        editBoxDiv.style.gridColumn = "1";
    }
    editBoxDiv.className = "editableBoxDiv";
    editBoxDiv.style.minHeight = window.getComputedStyle(LabelObject,null).height + "px"

    // Set the <input/> inside the <div/>'s properties
    editBox.style.width = LabelObject.offsetWidth  + "px";
    editBox.style.height = LabelObject.offsetHeight + "px";
    editBox.style.fontSize = window.getComputedStyle(LabelObject,null).getPropertyValue('font-size');
    editBox.style.fontFamily = window.getComputedStyle(LabelObject,null).getPropertyValue('font-family');
    editBox.style.fontWeight = window.getComputedStyle(LabelObject, null).fontWeight
    editBox.className = "editableBox"


    // Set the <p/>'s / Label Object's text to invisible
    LabelObject.style.color = "#fff0";
    console.log(LabelObject.id)

    editBox.focus();
    editBox.addEventListener("focusout", function(){
        setFocusOutEditableLabelProperties(editBoxDiv, LabelObject, ObjectToAddTo);
    })
    editBox.oninput = function(){
        if (editBox.value === "") {
            return;
        }
        if (editBox.value.length == 1) {
            editBox.value = editBox.value.toUpperCase()
        }
        if (Label.includes("_emptySectorSizeLabelValue")){
            if (editBox.value < 50){
                LabelObject.innerText = 50 + "px"
            }
            else{
                LabelObject.innerText = editBox.value + "px"
            }
        }
        else{
            LabelObject.innerText = editBox.value;
        }
        editBox.style.width = (LabelObject.offsetWidth) + "px";

    }
}

function setEditableLabelValue(row, column, Label, ObjectToAddTo){
    const textBox = document.getElementById(`${row}` + ';' +  `${column}` + ';' +  "_editableLabel");
    let LabelObject;
    if (ObjectToAddTo.includes("_emptySector")){
        let children = document.getElementById(ObjectToAddTo).children
        for (let i=0; i<children.length; i++){
            if (children[i].id === Label){
                LabelObject = children[i]
            }
        }
    }

    if (Label.includes("_emptySectorSizeLabelValue")){

        textBox.value = LabelObject.innerText.split("p")[0];

    }
    else
    {
        textBox.value = document.getElementById(`${Label}`).innerText;

    }

}

function setFocusOutEditableLabelProperties(editBoxDiv, LabelObject, ObjectToAddTo){
    if (LabelObject.id.includes("_emptySectorSizeLabelValue")) {
        let parentSector = document.getElementById(ObjectToAddTo)
        parentSector.style.height = LabelObject.innerText
    }
    LabelObject.style.color = "white"
    editBoxDiv.remove()
}

async function populate_boxes(){
    await fetch('http://localhost:3000/skill/get/all',{
        Method: 'GET',
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        },
        Body: {}
    }).then(function(response){
        response.json().then(function (value){
            populate_skill_list(value)
        })
    })

    await fetch('http://localhost:3000/project/get/all',{
        Method: 'GET',
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        },
        Body: {}
    }).then(function(response){
        response.json().then(function (value){
            populate_project_list(value)
        })
    })

    let port_id

    await fetch("http://localhost:3000/portfolio/get/to-edit",{
        method: "GET",
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        }
    }).then(function(response){
        console.log(response.json().then(function (value){
            port_id = value["portfolio_to_edit"];
            console.log(port_id)
        }).then(async function (){

            await fetch('http://localhost:3000/portfolio/get/skills?' + new URLSearchParams({
                "port_id" : port_id
            }),{
                Method: 'GET',
                Headers: {
                    Accept: 'application.json',
                    'Content-Type': 'application/json'
                },
                Body: {}
            }).then(function(response){
                response.json().then(function (value){
                    console.log("skills:" +value)
                })
            })


        }))
    })
}


function populate_skill_list(array){
    for (let i = 0; i < array.length; i++) {
        let skill_entry_div = document.createElement("div");
        let skill_entry = document.createElement("p");
        let skill_checkbox = document.createElement("input");

        skill_checkbox.type = "checkbox";
        skill_checkbox.style.gridRow = "1"
        skill_checkbox.style.gridColumn = "1"
        skill_checkbox.style.width = "20px";
        skill_checkbox.style.marginLeft = "auto"

        skill_checkbox.addEventListener("change", function(){
            selectedItem = "skill" + ";" + array[i].id;
        })

        skill_entry_div.className = "skill_entry_div"

        skill_entry.id = array[i].id;
        skill_entry.className = "skill_entry"
        skill_entry.innerText = "\nName: \n" + array[i].name + "\n\n";
        skill_entry.style.gridRow = "1"
        skill_entry.style.gridColumn = "1"


        skill_entry_div.appendChild(skill_entry);
        skill_entry_div.appendChild(skill_checkbox)
        skill_box.appendChild(skill_entry_div);
    }
}
function populate_project_list(array){
    for (let i = 0; i < array.length; i++) {
        let skill_entry_div = document.createElement("div");
        let skill_entry = document.createElement("p");
        let skill_checkbox = document.createElement("input");

        skill_checkbox.type = "checkbox";
        skill_checkbox.style.gridRow = "1"
        skill_checkbox.style.gridColumn = "1"
        skill_checkbox.style.width = "20px";
        skill_checkbox.style.marginLeft = "auto"

        skill_checkbox.addEventListener("change", function(){
            selectedItem = "project" + ";" + array[i].id;
            console.log(selectedItem)

        })

        skill_entry_div.className = "skill_entry_div"

        skill_entry.id = array[i].id;
        skill_entry.className = "skill_entry"
        skill_entry.innerText = "\nName: \n" + array[i].name + "\n\n";
        skill_entry.style.gridRow = "1"
        skill_entry.style.gridColumn = "1"


        skill_entry_div.appendChild(skill_entry);
        skill_entry_div.appendChild(skill_checkbox)
        project_box.appendChild(skill_entry_div);
    }
}



async function add_skill_to_portfolio(){

    let port_id
    await fetch("http://localhost:3000/portfolio/get/to-edit",{
        method: "GET",
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        }
    }).then(function(response){
        console.log(response.json().then(function (value){
            port_id = value["portfolio_to_edit"];
        }).then(async function (){
            await fetch('http://localhost:3000/portfolio/add/skill?' + new URLSearchParams({
                    "port_id" : port_id,
                    "skill_id" : selectedItem.split(";")[1],
                }
            ).toString(),{
                method: 'POST',
                Headers: {
                    Accept: 'application.json',
                    'Content-Type': 'application/json'
                }

            }).then(function(response){
                if(response.ok){
                }
            })
        }))
    })


}

async function add_project_to_portfolio(){

    let port_id
    await fetch("http://localhost:3000/portfolio/get/to-edit",{
        method: "GET",
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        }
    }).then(function(response){
        console.log(response.json().then(function (value){
            port_id = value["portfolio_to_edit"];
        }).then(async function (){
            await fetch('http://localhost:3000/portfolio/add/project?' + new URLSearchParams({
                    "port_id" : port_id,
                    "skill_id" : selectedItem.split(";")[1],
                }
            ).toString(),{
                method: 'POST',
                Headers: {
                    Accept: 'application.json',
                    'Content-Type': 'application/json'
                }

            }).then(function(response){
                if(response.ok){
                    response.json().then(function(json){
                    })
                }
            })
        }))
    })


}