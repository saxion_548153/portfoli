let portfolio_list
let portfolio_to_edit

window.onload = init;

async function init(){
    portfolio_list = document.getElementById('portfolio_list');
    await populate_list()
}


async function populate_list(){
    await fetch('http://localhost:3000/portfolio/get/all',{
        Method: 'GET',
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        },
        Body: {}
    }).then(function(response){
        response.json().then(function (value){
            populate_portfolio_edit_list(value)
        })
    })
}

function populate_portfolio_edit_list(array){
    for (let i = 0; i < array.length; i++) {
        let portfolio_edit_entry_div = document.createElement("div");
        let portfolio_edit_entry = document.createElement("p");
        let portfolio_edit_button = document.createElement("button");

        portfolio_edit_button.style.gridRow = "1"
        portfolio_edit_button.style.gridColumn = "1"
        portfolio_edit_button.style.width = "40px";
        portfolio_edit_button.style.height = "30px";
        portfolio_edit_button.style.marginLeft = "auto"
        portfolio_edit_button.innerText = "Edit"

        portfolio_edit_entry_div.className = "portfolio_edit_entry_div"

        portfolio_edit_entry.id = array[i].id;
        portfolio_edit_entry.className = "portfolio_edit_entry"
        portfolio_edit_entry.style.color = "white"
        portfolio_edit_entry.innerText = "\nName: \n" + array[i].name + "\n\n";
        portfolio_edit_entry.style.gridRow = "1"
        portfolio_edit_entry.style.gridColumn = "1"
        
        portfolio_edit_button.onclick = async function(){
            await set_portfolio_edit(portfolio_edit_entry.id)
            document.location.href = "../../Portfoli/pages/create.html";
        }

        portfolio_edit_entry_div.appendChild(portfolio_edit_entry);
        portfolio_edit_entry_div.appendChild(portfolio_edit_button)
        portfolio_list.appendChild(portfolio_edit_entry_div);
    }
}

async function set_portfolio_edit(id){
    await fetch('http://localhost:3000/portfolio/update/to-edit?' + new URLSearchParams({
        "id" : id,
    }), {
        method: 'PUT',
    }).then(function(response){
        if(response.ok){

        }
    })
}