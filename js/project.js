let project_list;
let new_project_form;
let project_edited;

let edit_project_name;
let edit_project_description;
let edit_project_organization
let edit_project_button

window.onload = init;

async function init(){

    project_list = document.getElementById("my_project_list");
    new_project_form = document.getElementById("new_project_form");

    edit_project_name = document.getElementById("edit_project_name");
    edit_project_description = document.getElementById("edit_project_description");
    edit_project_button = document.getElementById("edit_project_button");

    let create_project_button = document.getElementById("create_project_button");

    edit_project_button.onclick = function () {
        update_project_information()
    }

    create_project_button.onclick = async function () {
        await create_new_project()
    }


   await get_all_projects()

}


async function get_all_projects(){
    fetch('http://localhost:3000/project/get/all',{
        Method: 'GET',
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        },
        Body: {}
    }).then(function(response){
        response.json().then(function (value){
            populate_project_list(value)
            console.log(value)
        })
    })
}

async function create_new_project(){
    let project_name = document.getElementById("project_name")
    let project_description = document.getElementById("project_description")
    let project_organization = document.getElementById("project_organization")


    console.log()
    if (!project_name.value || !project_description.value || project_name.value.startsWith(" ")
     || project_description.value.startsWith(" ")) {
        alert("Please enter a valid name and description")
        return
    }

    let headers = new Headers()
    headers.append("Content-Type", "application/json")

    fetch('http://localhost:3000/project/add',{
        method: 'POST',
        Headers: headers,
        body: JSON.stringify({
            "project" : {
                "name" : project_name.value,
                "description" : project_description.value,
                "organization" : project_organization.value
            }
        })
    }).then(function(response){
        if(response.ok){
            project_name.value = ""
            project_description.value = ""
        }
    }).then(function(response){
        location.reload()
    })
}

async function delete_project(project_id){
    let headers = new Headers()

    fetch('http://localhost:3000/project/delete',{
        method: 'DELETE',
        Headers: headers,
        body: JSON.stringify({
            "id" : project_id
        })
    }).then(function(response){
        if(response.ok){
            location.reload()
        }
    })

}

function populate_project_list(array){
    for (let i = 0; i < array.length; i++) {
        let project_entry_div = document.createElement("div");
        let project_entry = document.createElement("p");
        let project_entry_delete_button = document.createElement("button");
        let project_entry_edit_button = document.createElement("button");

        project_entry_delete_button.className = "project_entry_delete_button"
        project_entry_delete_button.style.width = "25px";
        project_entry_delete_button.style.height = "25px";
        project_entry_delete_button.style.gridRow = "1"
        project_entry_delete_button.style.gridColumn = "1"
        project_entry_delete_button.innerText = "X"
        project_entry_delete_button.style.backgroundColor = "transparent";
        project_entry_delete_button.style.color = "white"


        project_entry_edit_button.className = "project_entry_edit_button"
        project_entry_edit_button.style.width = "25px";
        project_entry_edit_button.style.height = "25px";
        project_entry_edit_button.style.gridRow = "1"
        project_entry_edit_button.style.gridColumn = "1"
        project_entry_edit_button.innerText = "..."
        project_entry_edit_button.style.backgroundColor = "transparent";
        project_entry_edit_button.style.color = "white"



        project_entry_div.className = "project_entry_div"

        project_entry.id = array[i].id;
        project_entry.className = "project_entry"
        project_entry.innerText = "\nName: \n" + array[i].name + "\n\nDesc: \n" + array[i].description + "\n\n Org: \n" + array[i].organization + "\n\n";
        project_entry.style.gridRow = "1"
        project_entry.style.gridColumn = "1"

        project_entry_edit_button.onclick = async function (){
            project_edited =  project_entry.id;
            await set_fields_values()
        }

        project_entry_delete_button.onclick = async function(){
            await delete_project(project_entry.id)
            console.log("Tried to delete project number " + project_entry.id);
        }

        project_entry_div.appendChild(project_entry);
        project_entry_div.appendChild(project_entry_delete_button)
        project_entry_div.appendChild(project_entry_edit_button)
        project_list.appendChild(project_entry_div);
    }
}

async function set_fields_values(){
    let headers = new Headers()
    headers.append("Content-Type", "application/json")


    edit_project_name = document.getElementById("edit_project_name");
    edit_project_description = document.getElementById("edit_project_description");
    edit_project_organization = document.getElementById("edit_project_organization");

    await fetch('http://localhost:3000/project/get?' + new URLSearchParams({
        "id" : project_edited
        }
    ).toString(),{
        method: 'GET',
        Headers: headers,

    }).then(function(response){
        if(response.ok){
            response.json().then(function(json){
                edit_project_name.value = json[0].name;
                edit_project_description.value = json[0].description;
                edit_project_organization.value = json[0].organization;
            })
        }
    })
}

async function update_project_information(){
    edit_project_name = document.getElementById("edit_project_name");
    edit_project_description = document.getElementById("edit_project_description");
    edit_project_organization = document.getElementById("edit_project_organization");

    if (!edit_project_name.value  || !edit_project_description.value ){
        alert("Please enter a valid name and description")
        return
    }

    if (!project_edited || project_edited === 0){
        alert("Please select a project first")
        return
    }

    await fetch('http://localhost:3000/project/update?' + new URLSearchParams({
        "id" : project_edited,
        "name" : edit_project_name.value,
        "description" : edit_project_description.value,
        "organization" : edit_project_organization.value,
    }), {
        method: 'PUT',
    }).then(function(response){
        if(response.ok){
            edit_project_name.value = ""
            edit_project_description.value = ""
            project_edited = 0
            location.reload()
        }
        else{
            alert("Could not update project, please reselect the project")
        }
    })
}