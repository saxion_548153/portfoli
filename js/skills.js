let skill_list;
let new_skill_form;
let skill_edited;

let edit_skill_name;
let edit_skill_description;
let edit_skill_button

window.onload = init;

async function init(){

    skill_list = document.getElementById("my_skill_list");
    new_skill_form = document.getElementById("new_skill_form");

    edit_skill_name = document.getElementById("edit_skill_name");
    edit_skill_description = document.getElementById("edit_skill_description");
    edit_skill_button = document.getElementById("edit_skill_button");

    let create_skill_button = document.getElementById("create_skill_button");

    edit_skill_button.onclick = function () {
        update_skill_information()
    }

    create_skill_button.onclick = async function () {
        await create_new_skill()
    }


   await get_all_skills()

}


async function get_all_skills(){
    await fetch('http://localhost:3000/skill/get/all',{
        Method: 'GET',
        Headers: {
            Accept: 'application.json',
            'Content-Type': 'application/json'
        },
        Body: {}
    }).then(function(response){
        response.json().then(function (value){
            populate_skill_list(value)
        })
    })
}

async function create_new_skill(){
    let skill_name = document.getElementById("skill_name")
    let skill_description = document.getElementById("skill_description")

    console.log()
    if (!skill_name.value || !skill_description.value || skill_name.value.startsWith(" ")
     || skill_description.value.startsWith(" ")) {
        alert("Please enter a valid name and description")
        return
    }

    let headers = new Headers()
    headers.append("Content-Type", "application/json")

   await fetch('http://localhost:3000/skill/add',{
        method: 'POST',
        Headers: headers,
        body: JSON.stringify({
            "skill" : {
                "name" : skill_name.value,
                "description" : skill_description.value
            }
        })
    }).then(function(response){
        if(response.ok){
            skill_name.value = ""
            skill_description.value = ""
        }
    }).then(function(response){
        location.reload()
    })
}

async function delete_skill(skill_id){
    let headers = new Headers()

    await fetch('http://localhost:3000/skill/delete',{
        method: 'DELETE',
        Headers: headers,
        body: JSON.stringify({
            "id" : skill_id
        })
    }).then(function(response){
        if(response.ok){
            location.reload()
        }
    })

}

function populate_skill_list(array){
    for (let i = 0; i < array.length; i++) {
        let skill_entry_div = document.createElement("div");
        let skill_entry = document.createElement("p");
        let skill_entry_delete_button = document.createElement("button");
        let skill_entry_edit_button = document.createElement("button");

        skill_entry_delete_button.className = "skill_entry_delete_button"
        skill_entry_delete_button.style.width = "25px";
        skill_entry_delete_button.style.height = "25px";
        skill_entry_delete_button.style.gridRow = "1"
        skill_entry_delete_button.style.gridColumn = "1"
        skill_entry_delete_button.innerText = "X"
        skill_entry_delete_button.style.backgroundColor = "transparent";
        skill_entry_delete_button.style.color = "white"


        skill_entry_edit_button.className = "skill_entry_edit_button"
        skill_entry_edit_button.style.width = "25px";
        skill_entry_edit_button.style.height = "25px";
        skill_entry_edit_button.style.gridRow = "1"
        skill_entry_edit_button.style.gridColumn = "1"
        skill_entry_edit_button.innerText = "..."
        skill_entry_edit_button.style.backgroundColor = "transparent";
        skill_entry_edit_button.style.color = "white"



        skill_entry_div.className = "skill_entry_div"

        skill_entry.id = array[i].id;
        skill_entry.className = "skill_entry"
        skill_entry.innerText = "\nName: \n" + array[i].name + "\n\nDesc: \n" + array[i].description + "\n\n";
        skill_entry.style.gridRow = "1"
        skill_entry.style.gridColumn = "1"

        skill_entry_edit_button.onclick = async function (){
            skill_edited =  skill_entry.id;
            await set_fields_values()
        }

        skill_entry_delete_button.onclick = async function(){
            await delete_skill(skill_entry.id)
            console.log("Tried to delete skill number " + skill_entry.id);
        }

        skill_entry_div.appendChild(skill_entry);
        skill_entry_div.appendChild(skill_entry_delete_button)
        skill_entry_div.appendChild(skill_entry_edit_button)
        skill_list.appendChild(skill_entry_div);
    }
}

async function set_fields_values(){
    let headers = new Headers()
    headers.append("Content-Type", "application/json")


    edit_skill_name = document.getElementById("edit_skill_name");
    edit_skill_description = document.getElementById("edit_skill_description");

    await fetch('http://localhost:3000/skill/get?' + new URLSearchParams({
        "id" : skill_edited
        }
    ).toString(),{
        method: 'GET',
        Headers: headers,

    }).then(function(response){
        if(response.ok){
            response.json().then(function(json){
                edit_skill_name.value = json[0].name;
                edit_skill_description.value = json[0].description;
            })
        }
    })
}

async function update_skill_information(){
    edit_skill_name = document.getElementById("edit_skill_name");
    edit_skill_description = document.getElementById("edit_skill_description");

    if (!edit_skill_name.value  || !edit_skill_description.value ){
        alert("Please enter a valid name and description")
        return
    }

    if (!skill_edited || skill_edited === 0){
        alert("Please select a skill first")
        return
    }

    await fetch('http://localhost:3000/skill/update?' + new URLSearchParams({
        "id" : skill_edited,
        "name" : edit_skill_name.value,
        "description" : edit_skill_description.value,
    }), {
        method: 'PUT',
    }).then(function(response){
        if(response.ok){
            edit_skill_name.value = ""
            edit_skill_description.value = ""
            skill_edited = 0
            location.reload()
        }
        else{
            alert("Could not update skill, please reselect the skill")
        }
    })
}